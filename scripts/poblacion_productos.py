#!/usr/bin/python
import psycopg2
import random
conn = psycopg2.connect("dbname=electiva5DB user=postgres  password=123456")
cur = conn.cursor()
query = ''
for i in range(10000):
	sale_price = random.randint(10, 100)*1000
	datos = {
		'id':i+10,
		'name': 'Producto %s'%i,
		'description' : 'Producto %s'%i,
		'sale_price' : sale_price,
		'buy_price' : int(sale_price*0.8),
		'provider_id' : 1,
		'stock' : random.randint(1, 100)
	}
	query += ''' insert into Product(id, name, description, sale_price, buy_price, provider_id, stock) 
	values ({id},'{name}','{description}',{sale_price},{buy_price},{provider_id},{stock}); 
	'''.format(**datos)
cur.execute(query)
conn.commit()
cur.close()
conn.close()

/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import py.pol.una.ii.pw.annotation.LoginRequired;
import py.pol.una.ii.pw.data.BuyRepository;
import py.pol.una.ii.pw.mybatis.bean.BuyBean;
import py.pol.una.ii.pw.mybatis.bean.BuyDetailBean;
import py.pol.una.ii.pw.mybatis.bean.ProviderBean;
import py.pol.una.ii.pw.serviceImpl.BuyRegistration;
import py.pol.una.ii.pw.serviceImpl.MassiveBuyRegistration;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the Clients table.
 */

/**
 * 
 * @author gmarin
 *
 */
@Path("/buys")
@RequestScoped
@LoginRequired
public class BuyResourceRESTService {
    @Inject
    private Logger log;

    @EJB
    private BuyRegistration registration;
    
    @Inject
    private BuyRepository repository;
    
    @Inject
    private MassiveBuyRegistration massiveRegistration;
    
    /*@GET
    @Path("/paginable/{pageNumber:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Buy> listAllBuys(@PathParam("pageNumber") int pageNumber) {
        return repository.findAllOrderedById(pageNumber);
    }*/
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<BuyBean> listAllBuys() {
        return massiveRegistration.findAll();
    }
    
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public BuyBean findBuyById(@PathParam("id") long id) {
    	BuyBean Buy = massiveRegistration.findById(id);
        if (Buy == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return Buy;
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BuyBean updateBuy(BuyBean buy) {

    	massiveRegistration.updateBuy(buy);;
        
    	return buy;
    }
    
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteBuyById(@PathParam("id") long id){
        Response.ResponseBuilder builder = null;
        try{
        	massiveRegistration.deleteBuy(id);
            builder = Response.ok();
        }
        catch (Exception e){
            Map<String, String> responseObj = new HashMap<String, String>();
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    /**
     * Creates a new Buy from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Path("/init")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createBuy(ProviderBean provider) {

        Response.ResponseBuilder builder = null;

        try {

            registration.initBuy(provider);

            // Create an "ok" response
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }


    /**
     * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message. This can then be used
     * by clients to show violations.
     * 
     * @param violations A set of violations that needs to be reported
     * @return JAX-RS response containing all violations
     */
    private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
        log.fine("Validation completed. violations found: " + violations.size());

        Map<String, String> responseObj = new HashMap<String, String>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }

    @POST
    @Path("/addItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addItem(List<BuyDetailBean> detail) {

        Response.ResponseBuilder builder = null;

        try {

            registration.addItemBuy(detail);

            // Create an "ok" response
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    @DELETE
    @Path("/deleteItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteItem(List<Long> idBuyDetails) {

        Response.ResponseBuilder builder = null;

        try {

            registration.deleteItemBuy(idBuyDetails);

            // Create an "ok" response
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    @POST
    @Path("/confirm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmBuy() {

        Response builder = null;

        try {

            BuyBean buy = registration.confirmBuy();

            // Create an "ok" response
            builder = Response.ok(buy, MediaType.APPLICATION_JSON).build();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations()).build();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }

        return builder;
    }
    
    @POST
    @Path("/cancel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelBuy() {

        Response builder = null;

        try {

            registration.cancelBuy();

            // Create an "ok" response
            builder = Response.ok().build();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations()).build();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }

        return builder;
    }
    
    @POST
    @Consumes("multipart/form-data")
    @Path("/massiveBuy")
    @Produces("application/json")
    public Response uploadFile(@MultipartForm  MultipartFormDataInput input) throws IOException{

       String fileName = "compras.txt";
       
       Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
       List<InputPart> inputParts = uploadForm.get("file");
       
       for (InputPart inputPart : inputParts) {

           try {

               //convert the uploaded file to inputstream
               InputStream inputStream = inputPart.getBody(InputStream.class,null);


               OutputStream out = new FileOutputStream( new File(fileName) );
               int read;
               byte[] bytes = new byte[1024];

               while ( (read = inputStream.read(bytes) ) != -1) {
                   out.write(bytes, 0, read);
               }

               out.close();

           } catch (IOException e) {
               return Response.serverError().entity("Error : " + e.getMessage()).build();
           }

           try {
               FileReader fr = new FileReader( fileName );
               massiveRegistration.massiveBuyRegistration( fr );
               fr.close();

           }catch(Exception e){
               return Response.serverError().entity("Error : " + e.getMessage()).build();
           }
       }

       return Response.status(200).build();


   }
    
}
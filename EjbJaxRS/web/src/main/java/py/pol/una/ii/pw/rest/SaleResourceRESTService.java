/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import py.pol.una.ii.pw.annotation.LoginRequired;
import py.pol.una.ii.pw.data.SaleRepository;
import py.pol.una.ii.pw.mybatis.bean.ClientBean;
import py.pol.una.ii.pw.mybatis.bean.SaleBean;
import py.pol.una.ii.pw.mybatis.bean.SaleDetailBean;
import py.pol.una.ii.pw.serviceImpl.MassiveSaleRegistration;
import py.pol.una.ii.pw.serviceImpl.SaleRegistration;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the Clients table.
 */

/**
 * 
 * @author gmarin
 *
 */
@Path("/sales")
@RequestScoped
@LoginRequired
public class SaleResourceRESTService {
    @Inject
    private Logger log;

    @EJB
    private SaleRegistration registration;
    
    @EJB
    private MassiveSaleRegistration massiveRegistration;
    
    @Inject
    private SaleRepository repository;
    
   /* @GET
    @Path("/paginable/{pageNumber:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Sale> listAllPaginable(@PathParam("pageNumber") int pageNumber) {
        return repository.findAllOrderedById(pageNumber);
    }*/
    
    /**
     * Listar todas las ventas existentes
     * 
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<SaleBean> listAllSales() {
        return massiveRegistration.findAll();
    }
    
    /**
     * Buscar venta por id
     * 
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public SaleBean findSaleById(@PathParam("id") long id) {
    	
    	SaleBean sale = massiveRegistration.findById(id);
    	
    	return sale;
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public SaleBean updateSale(SaleBean sale) {

    	massiveRegistration.updateSale(sale);;
        
    	return sale;
    }
    
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSaleById(@PathParam("id") long id){
        Response.ResponseBuilder builder = null;
        try{
        	massiveRegistration.deleteSale(id);
            builder = Response.ok();
        }
        catch (Exception e){
            Map<String, String> responseObj = new HashMap<String, String>();
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    /**
     * Iniciar venta por cliente
     * 
     */
    @POST
    @Path("/init")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createSale(ClientBean client) {

        Response.ResponseBuilder builder = null;

        try {

            registration.initSale(client);

            // Create an "ok" response
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }


    /**
     * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message. This can then be used
     * by clients to show violations.
     * 
     * @param violations A set of violations that needs to be reported
     * @return JAX-RS response containing all violations
     */
    private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
        log.fine("Validation completed. violations found: " + violations.size());

        Map<String, String> responseObj = new HashMap<String, String>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }

    /**
     * Agregar item a la venta
     * 
     */
    @POST
    @Path("/addItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addItem(List<SaleDetailBean> detail) {

        Response.ResponseBuilder builder = null;

        try {

            registration.addItemSale(detail);

            // Create an "ok" response
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    /**
     * Eliminar item de la venta
     * 
     */
    @DELETE
    @Path("/deleteItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteItem(List<Long> idDetailSales) {

        Response.ResponseBuilder builder = null;

        try {

            registration.deleteItemSale(idDetailSales);

            // Create an "ok" response
            builder = Response.ok();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    /**
     * Confirmar venta
     * 
     */
    @POST
    @Path("/confirm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmSale() {

        Response builder = null;

        try {

            SaleBean sale = registration.confirmSale();

            // Create an "ok" response
            builder = Response.ok(sale, MediaType.APPLICATION_JSON).build();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations()).build();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }

        return builder;
    }
    
    /**
     * Cancelar venta
     * 
     */
    @POST
    @Path("/cancel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelSale() {

        Response builder = null;

        try {

            registration.cancelSale();

            // Create an "ok" response
            builder = Response.ok().build();
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations()).build();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj).build();
        }

        return builder;
    }
    
    /**
     * Realizar venta masiva
     * 
     */
    @POST
    @Path("/massiveSales")
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response uploadFile(@MultipartForm  MultipartFormDataInput input) throws IOException{

       String fileName = "ventas.txt";
    	
       Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
       List<InputPart> inputParts = uploadForm.get("file");
       
       for (InputPart inputPart : inputParts) {

           try {

               //convert the uploaded file to inputstream
               InputStream inputStream = inputPart.getBody(InputStream.class,null);


               OutputStream out = new FileOutputStream( new File(fileName) );
               int read;
               byte[] bytes = new byte[1024];

               while ( (read = inputStream.read(bytes) ) != -1) {
                   out.write(bytes, 0, read);
               }

               out.close();

           } catch (IOException e) {
               return Response.serverError().entity("Error : " + e.getMessage()).build();
           }

           try {
               FileReader fr = new FileReader( fileName );
               massiveRegistration.massiveSaleRegistration(fr );
               fr.close();

           }catch(Exception e){
               return Response.serverError().entity("Error : " + e.getMessage()).build();
           }
       }

       return Response.status(200).build();
    }
	
}

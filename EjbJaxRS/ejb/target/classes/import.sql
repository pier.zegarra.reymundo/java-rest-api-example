--
-- JBoss, Home of Professional Open Source
-- Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- You can use this file to load seed data into the database using SQL statements
insert into Registrant(id, name, email, phone_number) values (1, 'Alvaro Rodriguez', 'alvaro.rodriguez@radix.com.py', '+595961946427');
insert into Registrant(id, name, email, phone_number) values (2, 'John Smith', 'john.smith@mailinator.com', '2125551123');
insert into Registrant(id, name, email, phone_number) values (3, 'Gabriela Marin', 'gamarinb@gmail.com', '+595973631663');
insert into Registrant(id, name, email, phone_number) values (4, 'Matias Cabral', 'mcabral@gmail.com', '+595972123123')
insert into Usuario(id, username,password) values (1, 'gmarin', 'gmarin');

-- Insert Clients
insert into Client(id, name, phone_number, email, ruc, due) values (1,'Alvaro Danilo', '+595961946427','alvaro.rodriguez@radix.com.py','4617510-5', 50000)

-- Insert Provider
insert into Provider(id, name, phone_number, email, ruc) values (1,'Gabriela Marin', '+595973631663', 'gamarinb@gmail.com', '3810655-8')

-- Insert Product
insert into Product(id, name, description, sale_price, buy_price, provider_id, stock) values (1,'Teclado', 'Teclado para ordenador', 33000,35000, 1, 1000);
insert into Product(id, name, description, sale_price, buy_price, provider_id, stock) values (2,'Mouse', 'Mouse para ordenador', 10000,13000, 1, 1000);
insert into Product(id, name, description, sale_price, buy_price, provider_id, stock) values (3,'Pendrive', 'De 16 GB de capacidad', 40000,55000, 1, 1000);
insert into Product(id, name, description, sale_price, buy_price, provider_id, stock) values (4,'Monitor', 'De 32 pulgadas marca LG', 325000,450000, 1, 1000);
insert into Product(id, name, description, sale_price, buy_price, provider_id, stock) values (5,'Parlante', 'Parlantes para computadoras', 26000,40000, 1, 1000)
package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.SessionBean;

public interface SessionMapper{

    public Boolean isExist(Long id);

    public SessionBean getSessionById(Long id);
    
    public Long newSession(SessionBean session);
    
    public List<SessionBean> getAllSessions();
    
    public Long updateSession(SessionBean session);
    
    public void deleteSession(Long id);
    
    public SessionBean getSessionByToken(String token);
    

}
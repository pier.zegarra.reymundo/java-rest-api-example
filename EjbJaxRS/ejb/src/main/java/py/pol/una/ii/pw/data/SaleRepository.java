/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.data;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

import py.pol.una.ii.pw.model.Sale;
import py.pol.una.ii.pw.model.SaleDetail;

/**
 * 
 * @author gmarin
 *
 */
@ApplicationScoped
public class SaleRepository {

    @Inject
    private EntityManager em;

    public Sale findById(Long id) {
        return em.find(Sale.class, id);
    }
    
    public List<Sale> findAllOrderedById(int pageNumber) {
    	
    	int pageSize = 10;
        
    	CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sale> criteria = cb.createQuery(Sale.class);
        Root<Sale> saleRoot = criteria.from(Sale.class);
        criteria.select(saleRoot).orderBy(cb.asc(saleRoot.get("id")));
        return em.createQuery(criteria).setFirstResult((pageNumber-1)*pageSize).setMaxResults(pageSize).getResultList();
    }

    public List<Sale> findByIdClient(long idClient) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sale> criteria = cb.createQuery(Sale.class);
        Root<Sale> Sale = criteria.from(Sale.class);
        
        criteria.select(Sale).where(cb.equal(Sale.get("provider").get("id"), idClient));
        return em.createQuery(criteria).getResultList();
    }
    
    public List<Sale> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sale> criteria = cb.createQuery(Sale.class);
        Root<Sale> Sale = criteria.from(Sale.class);
        
        criteria.select(Sale);
        return em.createQuery(criteria).getResultList();
       
    }
    
    public List<SaleDetail> findSaleDetailByIdSale(Long idSale) {
    	  
    	  CriteriaBuilder cb = em.getCriteriaBuilder();
    	  
    	  CriteriaQuery<SaleDetail> q = cb.createQuery(SaleDetail.class);
    	  Root<SaleDetail> c = q.from(SaleDetail.class);
    	  ParameterExpression<Sale> p = cb.parameter(Sale.class);
    	 
    	  q.select(c).where(cb.equal(c.get("sale"), p));
    	  TypedQuery<SaleDetail> query = em.createQuery(q);
    	  query.setParameter(p, this.findById(idSale));
    	  List<SaleDetail> results = query.getResultList();
    	  
    	  return results;
      }

}

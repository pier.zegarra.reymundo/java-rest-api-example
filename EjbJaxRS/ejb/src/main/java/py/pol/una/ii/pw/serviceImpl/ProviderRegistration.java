/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.ProviderBean;
import py.pol.una.ii.pw.mybatis.manager.ProviderManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class ProviderRegistration {

    @Inject
    private Logger log;
    
    @EJB
    private ProviderManager providerManager;

    /**
     * Crear proveedor nuevo
     * 
     * @param provider
     * @return id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long createProvider(ProviderBean provider) {
        
    	log.info("Registering " + provider.getName());
    	
    	ProviderBean newProvider = new ProviderBean();
    	
    	newProvider.setEmail(provider.getEmail());
    	newProvider.setName(provider.getName());
    	newProvider.setPhoneNumber(provider.getPhoneNumber());
    	newProvider.setRuc(provider.getRuc());
    	
        Long providerId = providerManager.newProvider(newProvider);
        
    	log.info("Se ha registrado el provider con id" + providerId);
        
        return providerId;
    }
    
    /**
     * Actualizar datos del proveedor
     * 
     * @param provider_mod
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateProvider(ProviderBean provider_mod){
    	
    	log.info("Actualizando proveedor con id " + provider_mod.getId());
    	
    	ProviderBean provider = providerManager.getProviderById(provider_mod.getId());
    	
    	provider.setEmail(provider_mod.getEmail());
    	provider.setName(provider_mod.getName());
    	provider.setPhoneNumber(provider_mod.getPhoneNumber());
    	provider.setRuc(provider_mod.getRuc());
    	
    	providerManager.updateProvider(provider);
    	
    	log.info("Se actualizo proveedor con id " + provider.getId());

    }
    
    /**
     * Listar todos los proveedores existentes
     * 
     * @return list
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ProviderBean> findAll(){
    	
    	log.info("Buscando todos los proveedores existentes");
    	
    	List<ProviderBean> list = providerManager.getAllProviders();
    	
    	log.info("Se han obtenido " + list.size() + "proveedores.");
    	
    	return list;
    }
    
    /**
     * Buscar proveedor por id
     * 
     * @param id
     * @return provider
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ProviderBean findById(Long id){
    	
    	log.info("Buscando proveedor con id " + id);
    	
    	ProviderBean provider = providerManager.getProviderById(id);
    	
    	log.info("Se ha obtenido proveedor con id " + provider.getId());
    	
    	return provider;
    }
    
    /**
     * Eliminar proveedor por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteProvider(Long id){
    	
    	log.info("Eliminando proveedor con id " + id);
    	
    	providerManager.deleteProvider(id);
    	
    }
}
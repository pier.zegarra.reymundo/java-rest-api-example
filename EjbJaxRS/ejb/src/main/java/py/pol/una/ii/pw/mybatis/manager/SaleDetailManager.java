package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.SaleDetailBean;
import py.pol.una.ii.pw.mybatis.mapper.SaleDetailMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class SaleDetailManager implements SaleDetailMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
        	SaleDetailMapper saleDetailMapper = sqlSession.getMapper(SaleDetailMapper.class);
            exist = saleDetailMapper.getSaleDetailById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newSaleDetail(SaleDetailBean sale) {
        
        try {
        	SaleDetailMapper saleDetailMapper = sqlSession.getMapper(SaleDetailMapper.class);
            
        	saleDetailMapper.newSaleDetail(sale);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return sale.getId();
    }
    public SaleDetailBean getSaleDetailById(Long id) {
        
    	SaleDetailBean sale = null;
        try {
        	SaleDetailMapper saleDetailMapper = sqlSession.getMapper(SaleDetailMapper.class);
        	sale = saleDetailMapper.getSaleDetailById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (sale);
    }
   
    public List<SaleDetailBean> getAllSaleDetails(){
        
        List<SaleDetailBean> sales = null;
        try {
        	SaleDetailMapper saleDetailMapper = sqlSession.getMapper(SaleDetailMapper.class);
            sales = saleDetailMapper.getAllSaleDetails();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (sales);
    }
    public Long updateSaleDetail(SaleDetailBean sale_mod){
        
        Long id_sale = null;
        try {
        	SaleDetailMapper saleDetailMapper = sqlSession.getMapper(SaleDetailMapper.class);
            id_sale = saleDetailMapper.updateSaleDetail(sale_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_sale;
    }
    
    public void deleteSaleDetail(Long id){
        try {
        	SaleDetailMapper saleDetailMapper = sqlSession.getMapper(SaleDetailMapper.class);
        	saleDetailMapper.deleteSaleDetail(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}

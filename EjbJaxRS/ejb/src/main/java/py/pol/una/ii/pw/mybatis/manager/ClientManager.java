package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.ClientBean;
import py.pol.una.ii.pw.mybatis.mapper.ClientMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class ClientManager implements ClientMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            exist = clientMapper.getClientById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newClient(ClientBean client) {
        
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            
            clientMapper.newClient(client);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return client.getId();
    }
    public ClientBean getClientById(Long id) {
        
        ClientBean client = null;
        try {
            ClientMapper clienteMapper = sqlSession.getMapper(ClientMapper.class);
            client = clienteMapper.getClientById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (client);
    }
    public ClientBean findByEmail(String email){
        
        ClientBean client = null;
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            client = clientMapper.findByEmail(email);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (client);
    }
    public List<ClientBean> findAllOrderedByName(){
        
        List<ClientBean> clients = null;
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            clients = clientMapper.getAllClients();
            
            //Collections.sort(clientes);//, (a, b) -> a.getName().compareToIgnoreCase(b.getName()));
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (clients);
    }
    public List<ClientBean> getAllClients(){
        
        List<ClientBean> clients = null;
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            clients = clientMapper.getAllClients();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (clients);
    }
    public Long updateClient(ClientBean cliente_mod){
        
        Long id_cliente = null;
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            id_cliente = clientMapper.updateClient(cliente_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_cliente;
    }
    public void deleteClient(Long id){
        try {
            ClientMapper clientMapper = sqlSession.getMapper(ClientMapper.class);
            clientMapper.deleteClient(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}

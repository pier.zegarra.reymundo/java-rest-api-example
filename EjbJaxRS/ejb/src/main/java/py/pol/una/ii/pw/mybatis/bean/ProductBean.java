/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package py.pol.una.ii.pw.mybatis.bean;

/**
 * 
 * @author gmarin
 *
 */


public class ProductBean {

    private Long id;

    private String name;

    private String description;

    private Float salePrice;

    private Float buyPrice;

    private Long providerId;

    private int stock;
    
    public ProductBean(){
    	
    }

	public ProductBean(Long id, Float buyPrice, String description, String name,  Float salePrice, int stock, Long providerId
			) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.salePrice = salePrice;
		this.buyPrice = buyPrice;
		this.providerId = providerId;
		this.stock = stock;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }
    
    public Float getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Float buyPrice) {
        this.buyPrice = buyPrice;
    }
    
    public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}
	
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public void setProduct(ProductBean product){
    	this.id=product.getId();
    	this.name = product.getName();
    	this.description = product.getDescription();
    	this.providerId = product.getProviderId();
    	this.salePrice = product.getSalePrice();
    	this.buyPrice = product.getBuyPrice();
    	this.stock = product.getStock();
    }
}

/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.ClientBean;
import py.pol.una.ii.pw.mybatis.manager.ClientManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;

/**
 * 
 * @author gmarin
 *
 */

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class ClientRegistration {

    @Inject
    private Logger log;
    
    @EJB
    private ClientManager clienteManager;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createClient(ClientBean client) throws Exception {
    	
        log.info("Registering " + client.getName());

    	
    	ClientBean newClient = new ClientBean();
    	
    	newClient.setDue(client.getDue());
    	newClient.setEmail(client.getEmail());
    	newClient.setName(client.getName());
    	newClient.setPhoneNumber(client.getPhoneNumber());
    	newClient.setRuc(client.getRuc());   	
        
        clienteManager.newClient(newClient);
        
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateClient(ClientBean client_mod){
    	
    	log.info("Actualizando Client con id " + client_mod.getId());
	
    	ClientBean client = clienteManager.getClientById(client_mod.getId());
    	
    	client.setDue(client_mod.getDue());
    	client.setEmail(client_mod.getEmail());
    	client.setName(client_mod.getName());
    	client.setPhoneNumber(client_mod.getPhoneNumber());
    	client.setRuc(client_mod.getRuc());
    	
    	clienteManager.updateClient(client);
    	
    	log.info("Se ha actualizado cliente con id " + client.getId());
    	
    }
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteClient(Long id){
    	
    	log.info("Eliminando cliente con id " + id);
    	  
    	clienteManager.deleteClient(id);
    	
    	log.info("Se ha eliminado cliente con id " + id);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ClientBean> findAll() throws Exception {
        
    	log.info("Buscando todos los clientes existentes");
        
        List<ClientBean> list = clienteManager.getAllClients();
        
        log.info("Se han encontrado " + list.size() + " clientes");
        
        return list;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ClientBean findById(Long id) throws Exception {
    	
        log.info("Buscando cliente con id " + id);
        
        ClientBean client = clienteManager.getClientById(id);
        
        if(client!=null){
        	
            log.info("Se ha encontrado cliente con id " + client.getId());
            
            return client;
        }
        else{
        	
            log.info("No se ha encontrado cliente con id " + id);
            
            return null;

        }
        	
    }
}
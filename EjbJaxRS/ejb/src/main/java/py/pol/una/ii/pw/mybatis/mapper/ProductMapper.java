package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.ProductBean;

public interface ProductMapper{

    public Boolean isExist(Long id);
    
    public ProductBean getProductById(Long id);
    
    public Long newProduct(ProductBean sale);
    
    public List<ProductBean> getAllProducts();
    
    public Long updateProduct(ProductBean sale);
    
    public void deleteProduct(Long id);
    
    public List<ProductBean> getAllProductsByParam(String param);
    
}
package py.pol.una.ii.pw.mybatis.bean;

/**
 * 
 * @author alforro
 *
 */

public class BuyDetailBean {

	private Long id;

	private Long buyId;

	private Integer quantity;

	private Long productId;

	public BuyDetailBean(){
	    	
	    }

	public BuyDetailBean(Long id, Integer quantity, Long buyId, Long productId) {
			super();
			this.id = id;
			this.buyId = buyId;
			this.quantity = quantity;
			this.productId = productId;
		}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBuyId() {
		return buyId;
	}

	public void setBuyId(Long buyId) {
		this.buyId = buyId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

}

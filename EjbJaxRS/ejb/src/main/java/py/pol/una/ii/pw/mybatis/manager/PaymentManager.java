package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.PaymentBean;
import py.pol.una.ii.pw.mybatis.mapper.PaymentMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class PaymentManager implements PaymentMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            exist = paymentMapper.getPaymentById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newPayment(PaymentBean payment) {
        
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            
            paymentMapper.newPayment(payment);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return payment.getId();
    }
    public PaymentBean getPaymentById(Long id) {
        
        PaymentBean payment = null;
        try {
            PaymentMapper clienteMapper = sqlSession.getMapper(PaymentMapper.class);
            payment = clienteMapper.getPaymentById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (payment);
    }
    
    public List<PaymentBean> findAllOrderedByName(){
        
        List<PaymentBean> payments = null;
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            payments = paymentMapper.getAllPayments();
            
            //Collections.sort(clientes);//, (a, b) -> a.getName().compareToIgnoreCase(b.getName()));
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (payments);
    }
    public List<PaymentBean> getAllPayments(){
        
        List<PaymentBean> payments = null;
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            payments = paymentMapper.getAllPayments();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (payments);
    }
    public Long updatePayment(PaymentBean payment_mod){
        
        Long id_payment = null;
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            id_payment = paymentMapper.updatePayment(payment_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_payment;
    }
    public void deletePayment(Long id){
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            paymentMapper.deletePayment(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}

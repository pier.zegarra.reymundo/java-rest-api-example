package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.ProviderBean;
import py.pol.una.ii.pw.mybatis.mapper.ProviderMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class ProviderManager implements ProviderMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
        	ProviderMapper providerMapper = sqlSession.getMapper(ProviderMapper.class);
            exist = providerMapper.getProviderById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newProvider(ProviderBean provider) {
        
        try {
        	ProviderMapper providerMapper = sqlSession.getMapper(ProviderMapper.class);
            
        	providerMapper.newProvider(provider);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return provider.getId();
    }
    public ProviderBean getProviderById(Long id) {
        
    	ProviderBean provider = null;
        try {
        	ProviderMapper providerMapper = sqlSession.getMapper(ProviderMapper.class);
        	provider = providerMapper.getProviderById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (provider);
    }
   
    public List<ProviderBean> getAllProviders(){
        
        List<ProviderBean> providers = null;
        try {
        	ProviderMapper providerMapper = sqlSession.getMapper(ProviderMapper.class);
        	providers = providerMapper.getAllProviders();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (providers);
    }
    public Long updateProvider(ProviderBean provider_mod){
        
        Long id_provider = null;
        try {
        	ProviderMapper providerMapper = sqlSession.getMapper(ProviderMapper.class);
        	id_provider = providerMapper.updateProvider(provider_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_provider;
    }
    
    public void deleteProvider(Long id){
        try {
        	ProviderMapper providerMapper = sqlSession.getMapper(ProviderMapper.class);
        	providerMapper.deleteProvider(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}

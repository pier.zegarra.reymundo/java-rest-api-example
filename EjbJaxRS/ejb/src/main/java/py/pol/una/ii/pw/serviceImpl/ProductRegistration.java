/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.manager.ProductManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;
// The @Stateless annotation eliminates the need for manual transaction demarcation

@Stateless
public class ProductRegistration {

    @Inject
    private Logger log;

    @EJB
    private ProductManager productManager;

    /**
     * Crear producto nuevo
     * 
     * @param product
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createProduct(ProductBean product) {
        
    	log.info("Registering " + product.getName());
        
    	ProductBean newProduct = new ProductBean();
    	
    	newProduct.setBuyPrice(product.getBuyPrice());
    	newProduct.setDescription(product.getDescription());
    	newProduct.setName(product.getName());
    	newProduct.setProviderId(product.getProviderId());
    	newProduct.setSalePrice(product.getSalePrice());
    	newProduct.setStock(product.getStock());
    	
    	productManager.newProduct(newProduct);
    	
    	log.info("Se ha creado producto con id " + newProduct.getId());
    }
    
    /**
     * Actualizar datos de producto
     * 
     * @param product_mod
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateProduct(ProductBean product_mod){
    	
    	log.info("Actualizando producto con id " + product_mod.getId());
    	
    	ProductBean product = productManager.getProductById(product_mod.getId());
    	
    	product.setBuyPrice(product_mod.getBuyPrice());
    	product.setDescription(product_mod.getDescription());
    	product.setName(product_mod.getName());
    	product.setProviderId(product_mod.getProviderId());
    	product.setSalePrice(product_mod.getSalePrice());
    	product.setStock(product_mod.getStock());
    	
    	productManager.updateProduct(product);
    	
    	log.info("Se ha actualizado producto con id " + product.getId());
    }
    
    /**
     * Eliminar producto por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteProduct(Long id){
    	log.info("Eliminando producto con id " + id);
    	
    	productManager.deleteProduct(id);
    	
  	  	log.info("Se ha eliminado producto con id" + id);
    }
    
    /**
     * Buscar producto por parámetros
     * 
     * @param param
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ProductBean> findByParam(String param){
    	
    	List<ProductBean> products = productManager.getAllProductsByParam(param);
    	
    	return products;
    	
    }
    
    /**
     * Listar todos los productos existentes
     * 
     * @return list
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ProductBean> findAll(){
    	
    	log.info("Buscando todos los productos existentes");
    	
    	List<ProductBean> list = productManager.getAllProducts();
    	
    	log.info("Se han obtenido " + list.size() + " productos.");
    	
    	return list;
    }
    
    /**
     * Buscar producto por id
     * 
     * @param id
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ProductBean findById(Long id){
    	
    	log.info("Buscando producto con id " + id);
    	
    	ProductBean product = productManager.getProductById(id);
    	
    	log.info("Se ha obtenido producto con id " + product.getId());
    	
    	return product;
    }
    
}
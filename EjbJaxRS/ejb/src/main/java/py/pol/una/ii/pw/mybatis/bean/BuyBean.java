
package py.pol.una.ii.pw.mybatis.bean;

import java.util.Date;

/**
 * 
 * @author alforro
 *
 */
public class BuyBean{
    private Long id;
    
    private Long providerId;
    
    private Date date;

    private Float total;

    public BuyBean(){
    	
    }
    
	public BuyBean(Long id, Date date, Float total, Long providerId) {
		super();
		this.id = id;
		this.date = date;
		this.total = total;
		this.providerId = providerId;
	}
	
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public String toString() {
		return "Buy [id=" + id + ", provider=" + providerId + ", date=" + date + ", total=" + total + "]";
	}
	
	
  
}

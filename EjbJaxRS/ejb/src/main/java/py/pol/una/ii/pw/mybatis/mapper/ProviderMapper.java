package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.ProviderBean;

public interface ProviderMapper{

    public Boolean isExist(Long id);
    
    public ProviderBean getProviderById(Long id);
    
    public Long newProvider(ProviderBean sale);
    
    public List<ProviderBean> getAllProviders();
    
    public Long updateProvider(ProviderBean sale);
    
    public void deleteProvider(Long id);
}
package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.SessionBean;
import py.pol.una.ii.pw.mybatis.bean.UserBean;

public interface UserMapper{

    public Boolean isExist(Long id);

    public UserBean getUserById(Long id);
    
    public Long newUser(UserBean user);
    
    public List<UserBean> getAllUsers();
    
    public Long updateUser(UserBean user);
    
    public void deleteUser(Long id);
    
    public UserBean getUserByUsername(String username);
    
    public SessionBean login(String username, String password);
    
    public void logout(String username);
}
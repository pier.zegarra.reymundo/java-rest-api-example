
package py.pol.una.ii.pw.mybatis.bean;

import java.util.Date;


/**
 * 
 * @author gmarin
 *
 */
public class PaymentBean{
	
    private Long id;

    private Long clientId;

    private Float total;

    private Date date;
    
    public PaymentBean(){
    	
    }
    
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getClientId(){
    	return clientId;
    }

    public void setClientId(Long clientId){
    	this.clientId=clientId;
    }
    
    public void setTotal(Float total){
    	this.total = total;
    }
    
    public Float getTotal(){
    	return total;
    }
    public Date getDate(){
    	return date;
    }
    
    public void setDate(Date date){
    	this.date = date;
    }
}

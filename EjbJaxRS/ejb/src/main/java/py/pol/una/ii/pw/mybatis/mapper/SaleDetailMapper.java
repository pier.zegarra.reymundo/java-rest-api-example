package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.SaleDetailBean;

public interface SaleDetailMapper{

    public Boolean isExist(Long id);
    
    public SaleDetailBean getSaleDetailById(Long id);
    
    public Long newSaleDetail(SaleDetailBean sale);
    
    public List<SaleDetailBean> getAllSaleDetails();
    
    public Long updateSaleDetail(SaleDetailBean sale);
    
    public void deleteSaleDetail(Long id);
}
/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.BuyDetailBean;
import py.pol.una.ii.pw.mybatis.manager.BuyDetailManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;

/**
 * 
 * @author gmarin
 *
 */
// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class BuyDetailRegistration {

    @Inject
    private Logger log;

    @EJB
    private BuyDetailManager buyDetailManager;
    
    public void createBuyDetail(BuyDetailBean buyDetail) throws Exception {
        
    	log.info("Registrando detalle de compra con idProduct " + buyDetail.getProductId());
    	
    	BuyDetailBean newBuyDetail = new BuyDetailBean();
         
        newBuyDetail.setBuyId(buyDetail.getBuyId());
        newBuyDetail.setProductId(buyDetail.getProductId());
        newBuyDetail.setQuantity(buyDetail.getQuantity());

        buyDetailManager.newBuyDetail(newBuyDetail);
        
    }
    
    /**
     * Actualizar un detalle de compra
     * 
     * @param buyDetail
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateBuyDetail(BuyDetailBean buyDetail){
    	
    	log.info("Actualizar detalle de compra con id " + buyDetail.getId());
    	
    	BuyDetailBean newBuyDetail = buyDetailManager.getBuyDetailById(buyDetail.getId());
    	
    	newBuyDetail.setProductId(buyDetail.getProductId());
    	newBuyDetail.setQuantity(buyDetail.getQuantity());
    	newBuyDetail.setBuyId(buyDetail.getBuyId());
    	
    	buyDetailManager.updateBuyDetail(newBuyDetail);
	
    	log.info("Se ha actualizado detalle de compra con id " + newBuyDetail.getId());

    }
    
    /**
     * Listar todos los detalles de compras existentes
     * 
     * @return list
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<BuyDetailBean> findAll(){
    	
    	log.info("Buscando todos los detalles de compra");
    	
    	List<BuyDetailBean> list = buyDetailManager.getAllBuyDetails();
    	
    	log.info("Se han obtenido " + list.size() + "detalles de compra.");
    	
    	return list;
    }
    
    /**
     * Buscar detalle de compra por id
     * 
     * @param id
     * @return detail
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public BuyDetailBean findById(Long id){
    	
    	log.info("Buscando detalle de compra con id " + id);
    	
    	BuyDetailBean detail = buyDetailManager.getBuyDetailById(id);
    	
    	log.info("Se ha obtenido detalle de compra con id " + detail.getId());
    	
    	return detail;
    }
    
    /**
     * Eliminar detalle de compra por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteSaleDetail(Long id){
    	
    	log.info("Eliminando detalle de compra con id "+ id);
    	
    	buyDetailManager.deleteBuyDetail(id);
    	
    	log.info("Se ha eliminado detalle de compra con id "+ id);

    }
    
}
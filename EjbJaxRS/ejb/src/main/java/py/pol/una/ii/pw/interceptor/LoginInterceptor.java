package py.pol.una.ii.pw.interceptor;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;

import py.pol.una.ii.pw.annotation.LoginRequired;
import py.pol.una.ii.pw.mybatis.bean.SessionBean;
import py.pol.una.ii.pw.serviceImpl.SessionRegistration;

@LoginRequired
@Interceptor
public class LoginInterceptor {
	
	@Inject
    private Logger log;
    
    @EJB
    private SessionRegistration sessionRegistration;

    @Inject
    HttpServletRequest request;
    
    @AroundInvoke
    public Object aroundInvoke(InvocationContext ic) throws Exception {
        
        log.info("Activando interceptor..");
                        
        String token = request.getHeader("sessionId");
        
        if(token!=null){
            
            log.info("El token obtenido del header es: " + token);
            
            SessionBean session = sessionRegistration.findByToken(token);
            
            log.info("El usuario propietario del username es: " + session.getToken());
            
            if(session!=null){
                
                 return ic.proceed();
            }
        }
        
        return null;
    }

}

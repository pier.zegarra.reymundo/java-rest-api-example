package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.ibatis.session.SqlSession;
import py.pol.una.ii.pw.mybatis.bean.BuyDetailBean;
import py.pol.una.ii.pw.mybatis.mapper.BuyDetailMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class BuyDetailManager implements BuyDetailMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
        	BuyDetailMapper buyDetailMapper = sqlSession.getMapper(BuyDetailMapper.class);
            exist = buyDetailMapper.getBuyDetailById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newBuyDetail(BuyDetailBean buy) {
        
        try {
        	BuyDetailMapper buyDetailMapper = sqlSession.getMapper(BuyDetailMapper.class);
            
        	buyDetailMapper.newBuyDetail(buy);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return buy.getId();
    }
    public BuyDetailBean getBuyDetailById(Long id) {
        
    	BuyDetailBean buy = null;
        try {
        	BuyDetailMapper buyDetailMapper = sqlSession.getMapper(BuyDetailMapper.class);
        	buy = buyDetailMapper.getBuyDetailById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (buy);
    }
   
    public List<BuyDetailBean> getAllBuyDetails(){
        
        List<BuyDetailBean> buys = null;
        try {
        	BuyDetailMapper buyDetailMapper = sqlSession.getMapper(BuyDetailMapper.class);
            buys = buyDetailMapper.getAllBuyDetails();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (buys);
    }
    public Long updateBuyDetail(BuyDetailBean buy_mod){
        
        Long id_buy = null;
        try {
        	BuyDetailMapper buyDetailMapper = sqlSession.getMapper(BuyDetailMapper.class);
            id_buy = buyDetailMapper.updateBuyDetail(buy_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_buy;
    }
    
    public void deleteBuyDetail(Long id){
        try {
        	BuyDetailMapper buyDetailMapper = sqlSession.getMapper(BuyDetailMapper.class);
        	buyDetailMapper.deleteBuyDetail(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}

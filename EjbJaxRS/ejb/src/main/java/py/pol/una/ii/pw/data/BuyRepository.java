/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.data;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import java.util.List;

import py.pol.una.ii.pw.model.Buy;
import py.pol.una.ii.pw.model.BuyDetail;

/**
 * 
 * @author gmarin
 *
 */
@ApplicationScoped
public class BuyRepository {

    @Inject
    private EntityManager em;

    public Buy findById(Long id) {
        return em.find(Buy.class, id);
    }
    
    public List<Buy> findAllOrderedById(int pageNumber) {
    	
    	int pageSize = 10;
        
    	CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Buy> criteria = cb.createQuery(Buy.class);
        Root<Buy> compraRoot = criteria.from(Buy.class);
        criteria.select(compraRoot).orderBy(cb.asc(compraRoot.get("id")));
        return em.createQuery(criteria).setFirstResult((pageNumber-1)*pageSize).setMaxResults(10).getResultList();
    }

    public List<Buy> findByIdProvider(long idProvider) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Buy> criteria = cb.createQuery(Buy.class);
        Root<Buy> Buy = criteria.from(Buy.class);
        
        criteria.select(Buy).where(cb.equal(Buy.get("provider").get("id"), idProvider));
        return em.createQuery(criteria).getResultList();
    }
    
    public List<Buy> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Buy> criteria = cb.createQuery(Buy.class);
        Root<Buy> Buy = criteria.from(Buy.class);
        
        criteria.select(Buy);
        return em.createQuery(criteria).getResultList();
       
    }
    
    public List<BuyDetail> findBuyDetailByIdBuy(Long idBuy) {
    	  
    	  CriteriaBuilder cb = em.getCriteriaBuilder();
    	  
    	  CriteriaQuery<BuyDetail> q = cb.createQuery(BuyDetail.class);
    	  Root<BuyDetail> c = q.from(BuyDetail.class);
    	  ParameterExpression<Buy> p = cb.parameter(Buy.class);
    	  q.select(c).where(cb.equal(c.get("buy"), p));
    	  TypedQuery<BuyDetail> query = em.createQuery(q);
    	  query.setParameter(p, this.findById(idBuy));
    	  List<BuyDetail> results = query.getResultList();
    	  
    	  return results;
      }

}

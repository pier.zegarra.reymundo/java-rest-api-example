package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.mapper.ProductMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class ProductManager implements ProductMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            exist = productMapper.getProductById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newProduct(ProductBean sale) {
        
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            
        	productMapper.newProduct(sale);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return sale.getId();
    }
    public ProductBean getProductById(Long id) {
        
    	ProductBean product = null;
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
        	product = productMapper.getProductById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (product);
    }
   
    public List<ProductBean> getAllProducts(){
        
        List<ProductBean> products = null;
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
        	products = productMapper.getAllProducts();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (products);
    }
    public Long updateProduct(ProductBean product_mod){
        
        Long id_product = null;
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
        	id_product = productMapper.updateProduct(product_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_product;
    }
    
    public void deleteProduct(Long id){
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
        	productMapper.deleteProduct(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    public List<ProductBean> getAllProductsByParam(String param){
        
        List<ProductBean> products = null;
        try {
        	ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
        	products = productMapper.getAllProductsByParam(param);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (products);
    }
    
    
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}

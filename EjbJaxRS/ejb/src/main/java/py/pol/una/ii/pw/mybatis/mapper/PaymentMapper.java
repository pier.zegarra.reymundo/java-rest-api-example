package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.PaymentBean;

public interface PaymentMapper{

    public Boolean isExist(Long id);

    public PaymentBean getPaymentById(Long id);
    
    public Long newPayment(PaymentBean payment);
    
    public List<PaymentBean> getAllPayments();
    
    public Long updatePayment(PaymentBean payment);
    
    public void deletePayment(Long id);
}
package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.ClientBean;

public interface ClientMapper{

    public Boolean isExist(Long id);

    public ClientBean getClientById(Long id);

    public ClientBean findByEmail(String email);
    
    public Long newClient(ClientBean client);
    
    public List<ClientBean> getAllClients();
    
    public Long updateClient(ClientBean client);
    
    public void deleteClient(Long id);
}
package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.SaleBean;

public interface SaleMapper{

    public Boolean isExist(Long id);
    
    public SaleBean getSaleById(Long id);
    
    public Long newSale(SaleBean sale);
    
    public List<SaleBean> getAllSales();
    
    public Long updateSale(SaleBean sale);
    
    public void deleteSale(Long id);
    
}
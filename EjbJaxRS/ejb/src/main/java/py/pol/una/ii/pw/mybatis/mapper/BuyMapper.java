package py.pol.una.ii.pw.mybatis.mapper;

import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.BuyBean;

public interface BuyMapper{

    public Boolean isExist(Long id);
    
    public BuyBean getBuyById(Long id);
    
    public Long newBuy(BuyBean buy);
    
    public List<BuyBean> getAllBuys();
    
    public Long updateBuy(BuyBean buy);
    
    public void deleteBuy(Long id);
    
}
